decl
	integer fd0,fd1,fd2,fd3,fd4,fd5,fd6;
	string letter[20];
	string word[20];
enddecl
integer main()
{
	fd0 = Create("test.dat");
	print(fd0);
	
	fd1 = Open("test.dat");
	print(fd1);

	fd0 = Delete("test.dat");
	print(fd0);

	fd0 = Close(fd1);
	print(fd1);

	fd0 = Delete("test.dat");
	print(fd0);

	fd0 = Delete("test.dat");
	print(fd0);
	
	fd3 = Open("test.dat");
	print(fd3);

	return 0;
}
