decl
integer prime(integer n), a;
enddecl

integer prime(integer n)
{
	integer res, i;
	res = 1;
	if (n==0) then
		res =0;
	endif;
	if (n==1) then
		res = 0;
	endif;
	if (n%2==0) then
		res = 0;
	endif;
	if (n==2) then
		res = 1;
	endif;
	i = n-1;
	while (i > 2) do
		if (n%i == 0) then
			res = 0;
			break;
		endif;
		i = i - 1;
	endwhile;
	return res;
}

integer main()
{
	integer x, j;
	read(a);
	j = a;
	while (j>=0) do
		x = prime(j);
		if (x==1) then
			print(j);
		endif;
		j = j-1;
	endwhile;
	return 0;
}
	
	
